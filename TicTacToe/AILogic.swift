//
//  AILogic.swift
//  TicTacToe
//
//  Created by Ed Allonby on 14/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Handles AI Behaviour, such as where to place moves.

internal class AILogic
{
	private var isAiDeciding = false

	var IsAIDeciding: Bool
	{
		return isAiDeciding
	}

	internal func makeMove(plays: [Int:Int]) -> Int?
	{
		isAiDeciding = true

		// We (the computer) have two in a row
		if let result = rowCheck(plays, value: 0)
		{
			println("comp has two in a row")
			var whereToPlayResult = attemptLogicalMove(result[0], pattern: result[1])

			if !isOccupied(plays, spot: whereToPlayResult)
			{
				isAiDeciding = false
				return whereToPlayResult
			}
		}

		// They (the player) have two in a row
		if let result = rowCheck(plays, value: 1)
		{
			var whereToPlayResult = attemptLogicalMove(result[0], pattern: result[1])

			if !isOccupied(plays, spot: whereToPlayResult)
			{
				isAiDeciding = false
				return whereToPlayResult
			}
		}

		if !isOccupied(plays, spot: 4)
		{
			isAiDeciding = false
			return 4
		}

		if let cornerAvailable = firstAvailable(plays, isCorner: true)
		{
			isAiDeciding = false
			return cornerAvailable
		}

		if let sideAvailable = firstAvailable(plays, isCorner: false)
		{
			isAiDeciding = false
			return sideAvailable
		}

		isAiDeciding = false

		return nil
	}

	/// Attempts to find a logical place to play a move
	private func attemptLogicalMove(location: String, pattern: String) -> Int
	{
		var leftPattern = "011"
		var rightPattern = "110"
		var middlePattern = "101"
		switch location
		{
			case "top":
				if pattern == leftPattern
				{
					return 0
				}
				else if pattern == rightPattern
				{
					return 2
				}
				else
				{
					return 1
				}
			case "bottom":
				if pattern == leftPattern
				{
					return 6
				}
				else if pattern == rightPattern
				{
					return 8
				}
				else
				{
					return 7
				}
			case "left":
				if pattern == leftPattern
				{
					return 0
				}
				else if pattern == rightPattern
				{
					return 6
				}
				else
				{
					return 3
				}
			case "right":
				if pattern == leftPattern
				{
					return 2
				}
				else if pattern == rightPattern
				{
					return 8
				}
				else
				{
					return 5
				}
			case "middleVert":
				if pattern == leftPattern
				{
					return 1
				}
				else if pattern == rightPattern
				{
					return 7
				}
				else
				{
					return 4
				}
			case "middleHorz":
				if pattern == leftPattern
				{
					return 3
				}
				else if pattern == rightPattern
				{
					return 5
				}
				else
				{
					return 4
				}
			case "diagLeftRight":
				if pattern == leftPattern
				{
					return 0
				}
				else if pattern == rightPattern
				{
					return 8
				}
				else
				{
					return 4
				}
			case "diagRightLeft":
				if pattern == leftPattern
				{
					return 2
				}
				else if pattern == rightPattern
				{
					return 6
				}
				else
				{
					return 4
				}

			default:
				return 4
		}
	}

	private func isOccupied(plays: [Int:Int], spot: Int) -> Bool
	{
		println("occupied \(spot)")
		if plays[spot] != nil
		{
			return true
		}
		return false
	}

	private func rowCheck(plays: [Int:Int], value: Int) -> [String]?
	{
		var acceptableFinds = ["011", "110", "101"]
		var findFuncs = [checkBottom, checkMiddleAcross, checkTop, checkLeft, checkMiddleDown, checkRight, checkDiagLeftRight, checkDiagRightLeft]

		for algorithm in findFuncs
		{
			var algorithmResults = algorithm(plays, value: value)
			var findPattern = find(acceptableFinds, algorithmResults[1])
			if findPattern != nil
			{
				return algorithmResults
			}
		}

		return nil
	}

	private func checkThis(#value: Int) -> [String]
	{
		return ["right", "0"]
	}

	private func firstAvailable(plays: [Int:Int], isCorner: Bool) -> Int?
	{
		var spots = isCorner ? [0, 2, 6, 8] : [1, 3, 5, 7]

		for spot in spots
		{
			println("checking \(spot)")

			if !isOccupied(plays, spot: spot)
			{
				println("not occupied \(spot)")
				return spot
			}
		}

		return nil
	}

	private func checkBottom(plays: [Int:Int], value: Int) -> [String]
	{
		return ["bottom", checkFor(plays, value: value, inList: [6, 7, 8])]
	}

	private func checkMiddleAcross(plays: [Int:Int], value: Int) -> [String]
	{
		return ["middleHorz", checkFor(plays, value: value, inList: [3, 4, 5])]
	}

	private func checkTop(plays: [Int:Int], value: Int) -> [String]
	{
		return ["top", checkFor(plays, value: value, inList: [0, 1, 2])]
	}

	private func checkLeft(plays: [Int:Int], value: Int) -> [String]
	{
		return ["left", checkFor(plays, value: value, inList: [0, 3, 6])]
	}

	private func checkMiddleDown(plays: [Int:Int], value: Int) -> [String]
	{
		return ["middleVert", checkFor(plays, value: value, inList: [1, 4, 7])]
	}

	private func checkRight(plays: [Int:Int], value: Int) -> [String]
	{
		return ["right", checkFor(plays, value: value, inList: [2, 5, 8])]
	}

	private func checkDiagLeftRight(plays: [Int:Int], value: Int) -> [String]
	{
		return ["diagRightLeft", checkFor(plays, value: value, inList: [2, 4, 6])]
	}

	private func checkDiagRightLeft(plays: [Int:Int], value: Int) -> [String]
	{
		return ["diagLeftRight", checkFor(plays, value: value, inList: [0, 4, 8])]
	}

	private func checkFor(plays: [Int:Int], value: Int, inList: [Int]) -> String
	{
		var conclusion = ""
		for cell in inList
		{
			if plays[cell] == value
			{
				conclusion += "1"
			}
			else
			{
				conclusion += "0"
			}
		}

		return conclusion
	}
}