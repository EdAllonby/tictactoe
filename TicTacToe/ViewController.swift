//
//  ViewController.swift
//  TicTacToe
//
//  Created by Ed Allonby on 14/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
	@IBOutlet var ticTacImage1: UIImageView!
	@IBOutlet var ticTacImage2: UIImageView!
	@IBOutlet var ticTacImage3: UIImageView!
	@IBOutlet var ticTacImage4: UIImageView!
	@IBOutlet var ticTacImage5: UIImageView!
	@IBOutlet var ticTacImage6: UIImageView!
	@IBOutlet var ticTacImage7: UIImageView!
	@IBOutlet var ticTacImage8: UIImageView!
	@IBOutlet var ticTacImage9: UIImageView!

	@IBOutlet var resetBtn: UIButton!
	@IBOutlet var userMessage: UILabel!

	var plays = [Int: Int]()
	var done = false

	var ticTacImages = [UIImageView]()

	var aiLogic = AILogic()
	var gameLogic = GameLogic()

	override func viewDidLoad()
	{
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.

		ticTacImages = [ticTacImage1, ticTacImage2, ticTacImage3, ticTacImage4, ticTacImage5, ticTacImage6, ticTacImage7, ticTacImage8, ticTacImage9]

		for imageView in ticTacImages
		{
			imageView.userInteractionEnabled = true
			imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "imageClicked:"))
		}
	}

	//Gesture Recogniser method
	func imageClicked(reco: UITapGestureRecognizer)
	{
		var imageViewTapped = reco.view as UIImageView
		println("The image \(imageViewTapped.tag) was tapped")
		println(plays[imageViewTapped.tag])
		println("Ai is deciding: \(aiLogic.IsAIDeciding)")
		println(done)

		if plays[imageViewTapped.tag] == nil && !aiLogic.IsAIDeciding && !done
		{
			setImageForSpot(imageViewTapped.tag, player: .UserPlayer)
		}

		var moveResult = gameLogic.didSomeoneWin(plays)

		if (moveResult.isWon)
		{
			winHandler(moveResult.winner)
		}
		else
		{
			aiTurn()
		}
	}

	@IBAction func resetBtnClicked(sender: UIButton)
	{
		done = false
		resetBtn.hidden = true
		userMessage.hidden = true
		reset()
	}

	func setImageForSpot(spot: Int, player: Player)
	{
		var playerMark = player == .UserPlayer ? "x" : "o"
		println("setting player \(player.rawValue) spot \(spot)")
		plays[spot] = player.rawValue
		ticTacImages[spot].image = UIImage(named: playerMark)
	}

	func winHandler(winner: String)
	{
		userMessage.hidden = false
		userMessage.text = "Looks like \(winner) won!"
		resetBtn.hidden = false;
		done = true;
	}

	func reset()
	{
		plays = [:]
		ticTacImage1.image = nil
		ticTacImage2.image = nil
		ticTacImage3.image = nil
		ticTacImage4.image = nil
		ticTacImage5.image = nil
		ticTacImage6.image = nil
		ticTacImage7.image = nil
		ticTacImage8.image = nil
		ticTacImage9.image = nil
	}

	func handleAIMove(whereToPlayResult: Int)
	{
		setImageForSpot(whereToPlayResult, player: .ComputerPlayer)

		var result = gameLogic.didSomeoneWin(plays)

		if (result.isWon)
		{
			winHandler(result.winner)
		}
	}

	func aiTurn()
	{
		if done
		{
			return
		}

		var moveToMake = aiLogic.makeMove(plays);

		if (moveToMake?.value == nil)
		{
			userMessage.hidden = false
			userMessage.text = "Looks like it was a tie!"

			reset()
		}
		else
		{
			handleAIMove(moveToMake!)
		}
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}