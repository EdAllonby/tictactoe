//
//  gameLogic.swift
//  TicTacToe
//
//  Created by Ed Allonby on 14/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Used to determine winning moves in the game

class GameLogic
{
	/// Decides whether someone won
	/// Returns the winners name and a flag of true if won
	/// returns a flag of false if not won
	func didSomeoneWin(plays: [Int:Int]) -> (isWon:Bool, winner:String)
	{
		var youWin = 1
		var theyWin = 0
		var whoWon = ["I": 0, "you": 1]

		for (key, value) in whoWon
		{
			// TODO: Swift doesn't like long-arsed if-statements, therefore lots of || statements can't be used hence the if statements being split up. I agree. Make this logic nicer.
			if ((plays[6] == value && plays[7] == value && plays[8] == value))
			{
				return (true, key)
			}

			if ((plays[3] == value && plays[4] == value && plays[5] == value))
			{
				return (true, key)
			}

			if ((plays[0] == value && plays[1] == value && plays[2] == value))
			{
				return (true, key)
			}

			if ((plays[6] == value && plays[3] == value && plays[0] == value))
			{
				return (true, key)
			}

			if ((plays[7] == value && plays[4] == value && plays[1] == value))
			{
				return (true, key)
			}

			if ((plays[8] == value && plays[5] == value && plays[2] == value))
			{
				return (true, key)
			}

			if ((plays[6] == value && plays[4] == value && plays[2] == value))
			{
				return (true, key)
			}

			if ((plays[8] == value && plays[4] == value && plays[0] == value))
			{
				return (true, key)
			}
		}

		return (false, "Nobody won this turn")
	}
}